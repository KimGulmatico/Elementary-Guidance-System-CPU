$(function(){

    $("#regBtn").click(function()
	{		
        slidetoRegister();
		$('#regwho').val('student');
		$('#id').val('');
		$('#pass').val('');
		$('#cfrmpass').val('');
    });

	$("#regBtnTeacher").click(function()
	{		
       slidetoRegister();
	   $('#regwho').val('teacher');
	   $('#id').val('');
	   $('#pass').val('');
	   $('#cfrmpass').val('');
    });
	
	function slidetoRegister()
	{
		$("#slide").addClass("slideleft");
		$("#slide2").addClass("slideleft2");
		$("#logdiv").addClass("logindiv2");
	}

	$("#imgBack").click(function()
	{		
		$("#slide").removeClass("slideleft");
		$("#slide2").removeClass("slideleft2");
		$("#logdiv").removeClass("logindiv2");
	});
	
	$("#regIDPassForm").submit(function( event ) 
	{		
		if($('#id').val()!="" && $('#pass').val() != "" && $('#cfrmpass').val() != "" && ($('#regwho').val() == "student" || $('#regwho').val() == "teacher"))
		{
			var id = $('#id').val();
			var pass = $('#pass').val();
			var cfrmpass = $('#cfrmpass').val();

			var forid = /^(?=.*[A-Za-z])[A-Za-z\d]{3,}$/i;
			var forpass = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/i;

			if(!forid.test(id) || !forpass.test(pass) || !forpass.test(cfrmpass))
			{
				if($('#pass').val() != $('#cfrmpass').val())
				{
					alert("Password and confirm password must be same");
					event.preventDefault();
				}
				event.preventDefault();
			}
	
		}
		else
		{ 
			alert("Fill all fields first");
			event.preventDefault();
		}
	});
	
	$("#regLoginForm").submit(function( event ) 
	{		
		if($('#logID').val()!="" && $('#logPassword').val() != "")
		{
			//var id = $('#logID').val();
			//var pass = $('#logPassword').val();

			//var forid = new RegExp('\b\d{2}[-]?\d{4}[-]?\d{2}\b');
			//var forpass = new RegExp('(?!.* ).{8,20}$');
			
			
			//if(!forid.test(id) && !forpass.test(pass))
			//{
			//	event.preventDefault();
			//}
		}
		else
		{ 
			alert("Fill all fields first");
			event.preventDefault();
		}
	});
});