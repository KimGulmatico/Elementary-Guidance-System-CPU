﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Runtime.InteropServices;
using System.Windows.Interop;
using System.Windows.Media.Animation;
using CefSharp;
using System.Threading;


namespace ElementaryGuidanceSystem
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const int GWL_STYLE = -16;
        private const int WS_SYSMENU = 0x80000;
        [DllImport("user32.dll", SetLastError = true)]
        private static extern int GetWindowLong(IntPtr hWnd, int nIndex);
        [DllImport("user32.dll")]
        private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        Thickness currentslidermargin = new Thickness(0, 0, -1220, 0);
        Double addedWidth;

        bool cefReferralsLoaded = false;
        bool cefLoginLoaded = false;

        public MainWindow()
        {
            var settings = new CefSettings();
            settings.CachePath = "cache";
            settings.DisableGpuAcceleration();
            Cef.Initialize(settings);
            InitializeComponent();
            this.DataContext = this;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Style _style = null;
            _style = (Style)FindResource("MyStyle");
            this.Style = _style;
            var hwnd = new WindowInteropHelper(this).Handle;
            SetWindowLong(hwnd, GWL_STYLE, GetWindowLong(hwnd, GWL_STYLE) & ~WS_SYSMENU);

            cefStudents.BrowserSettings = new BrowserSettings(){OffScreenTransparentBackground = false};
            cefStudents.MenuHandler = new MenuHandler();
            cefLogin.MenuHandler = new MenuHandler();
            cefHome.MenuHandler = new MenuHandler();
            cefReferrals.MenuHandler = new MenuHandler();
            cefAssessment.MenuHandler = new MenuHandler();
            cefDatatransfer.MenuHandler = new MenuHandler();
            cefGrades.MenuHandler = new MenuHandler();
            cefTeachers.MenuHandler = new MenuHandler();
            cefStatistics.MenuHandler = new MenuHandler();
            userLogin();
            TheEnclosingMethod();

        }
        public async void TheEnclosingMethod()
        {
            await Task.Delay(1000);
            if (cefReferralsLoaded)
            {
                bool guard = cefReferrals.CanExecuteJavascriptInMainFrame;
                if (guard)
                {
                    var task = this.cefReferrals.EvaluateScriptAsync("referCount();");
                    //task.ContinueWith(t =>
                    //{
                    //    if (!t.IsFaulted)
                    //    {
                    //        var response = t.Result;
                    //    var EvaluateJavaScriptResult = response.Success ? (response.Result ?? "null") : response.Message;
                    // MessageBox.Show(EvaluateJavaScriptResult.ToString());
                    //MessageBox.Show((string)response.Result);
                    //    }
                    //}, TaskScheduler.FromCurrentSynchronizationContext());
                    var res = task.Result;
                    // MessageBox.Show((string)res.Result);
                    if ((string)res.Result != null)
                    {
                        if (int.Parse((string)res.Result) != 0)
                        {
                            containerNewRef.Visibility = Visibility.Visible;
                            lnewReferrrals.Content = (string)res.Result;
                        }
                        else
                        {
                            containerNewRef.Visibility = Visibility.Hidden;
                        }
                    }
                }
            }
            TheEnclosingMethod();
        }

        public async void userLogin()
        {
            await Task.Delay(1000);
            if (cefLoginLoaded)
            {
                bool guard = cefLogin.CanExecuteJavascriptInMainFrame;
                if (guard)
                {
                    var task = this.cefLogin.EvaluateScriptAsync("loginSuccess();");
                    var res = task.Result;

                    if ((string)res.Result != null)
                    {
                        var task2 = this.cefLogin.EvaluateScriptAsync("serveripadrs();");
                        var res2 = task2.Result;
                        string srv = (string)res2.Result;

                        cefHome.Load("http://" + srv + "/ElementaryGuidanceWeb/phpdesktop/dashboards/dashboard.php");
                        cefStudents.Load("http://" + srv + "/ElementaryGuidanceWeb/phpdesktop/studentsearch");
                        cefTeachers.Load("http://" + srv + "/ElementaryGuidanceWeb/phpdesktop/teachers");
                        cefReferrals.Load("http://" + srv + "/ElementaryGuidanceWeb/phpdesktop/referrals.html");
                        cefAssessment.Load("http://" + srv + "/ElementaryGuidanceWeb/phpdesktop/assessment.html");
                        cefStatistics.Load("http://" + srv + "/ElementaryGuidanceWeb/phpdesktop/statistics.html");
                        cefDatatransfer.Load("http://" + srv + "/ElementaryGuidanceWeb/phpdesktop/dataTransfer.html");
                        cefGrades.Load("http://" + srv + "/ElementaryGuidanceWeb/phpdesktop/grades.html");

                        if (cefHome.WebBrowser.IsLoading == false && cefStudents.WebBrowser.IsLoading == false && cefTeachers.WebBrowser.IsLoading == false && cefReferrals.WebBrowser.IsLoading == false && cefAssessment.WebBrowser.IsLoading == false  && cefStatistics.WebBrowser.IsLoading == false && cefDatatransfer.WebBrowser.IsLoading == false && cefGrades.WebBrowser.IsLoading == false)
                        {
                            await Task.Delay(5000);
                            gridLogin.Visibility = Visibility.Hidden;
                            cefLoginLoaded = false;
                            RunStoryboardGridItemLeft(gridItem0);
                        }
                        
                    }
                }

            }
            userLogin();

        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void minimizeButton_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = System.Windows.WindowState.Minimized;
        }

        private void restoreButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.WindowState == System.Windows.WindowState.Maximized)
            {
                this.WindowState = System.Windows.WindowState.Normal;
            }
            else
            {
                this.WindowState = System.Windows.WindowState.Maximized;
            }
        }

        private void Window_StateChanged(object sender, EventArgs e)
        {
            if (this.WindowState == System.Windows.WindowState.Maximized)
            {
                this.restoreButton.Content = "2";
                this.maingrid.Margin = new Thickness(0, 8, 0, 0);
                this.cefStudents.Margin = new Thickness(0,0,8,0);
                this.cefReferrals.Margin = new Thickness(0, 0, 8, 0);
                this.cefAssessment.Margin = new Thickness(0, 0, 8, 0);
                this.cefStatistics.Margin = new Thickness(0, 0, 8, 0);
                this.cefDatatransfer.Margin = new Thickness(0, 0, 8, 0);
                this.cefGrades.Margin = new Thickness(0, 0, 8, 0);
                this.cefTeachers.Margin = new Thickness(0, 0, 8, 0);
                this.cefHome.Margin = new Thickness(0, 0, 8, 0); 
                this.cefLogin.Margin = new Thickness(0, 0, 8, 8);;
                this.closeButton.Margin = new Thickness(0, 0, 6, 0);
                this.restoreButton.Margin = new Thickness(0, 0, 52, 0);
                this.minimizeButton.Margin = new Thickness(0, 0, 88, 0);


            }
            if (this.WindowState == System.Windows.WindowState.Normal)
            {
                this.restoreButton.Content = "1";
                this.maingrid.Margin = new Thickness(0, 0, 0, 0);
                this.cefStudents.Margin = new Thickness(0, 0, 0, 0);
                this.cefReferrals.Margin = new Thickness(0, 0, 0, 0);
                this.cefAssessment.Margin = new Thickness(0, 0, 0, 0);
                this.cefStatistics.Margin = new Thickness(0, 0, 0, 0);
                this.cefDatatransfer.Margin = new Thickness(0, 0, 0, 0);
                this.cefGrades.Margin = new Thickness(0, 0, 0, 0);
                this.cefTeachers.Margin = new Thickness(0, 0, 0, 0);
                this.cefHome.Margin = new Thickness(0, 0, 0, 0);
                this.cefLogin.Margin = new Thickness(0, 0, 0, 0);
                this.closeButton.Margin = new Thickness(0, 0, 0, 0);
                this.restoreButton.Margin = new Thickness(0, 0, 46, 0);
                this.minimizeButton.Margin = new Thickness(0, 0, 82, 0);

            }

        }
        Storyboard sb;
        Storyboard navsb;
        Storyboard gridsb;

        public void RunStoryboard(string Storyboard)
        {
            sb = FindResource(Storyboard) as Storyboard;
            sb.Begin();
        }

        public void RunStoryboardNavLeft()
        {
            navsb = FindResource("slideL") as Storyboard;
            (navsb.Children[0] as DoubleAnimation).To = addedWidth;
            navsb.Begin();
        }

        public void RunStoryboardNavRight()
        {
            navsb = FindResource("slideR") as Storyboard;
            (navsb.Children[0] as DoubleAnimation).To = this.ActualWidth;
            navsb.Begin();
        }

        public void RunStoryboardNavLockLeft()
        {
            navsb = FindResource("slideLlockwidth") as Storyboard;
            (navsb.Children[0] as DoubleAnimation).To = addedWidth;
            navsb.Begin();
        }

        public void RunStoryboardNavLockRight()
        {
            navsb = FindResource("slideRlockwidth") as Storyboard;
            (navsb.Children[0] as DoubleAnimation).To = this.ActualWidth;
            navsb.Begin();
        }

        public void RunStoryboardGridItemLeft(Grid grid)
        {
            gridsb = FindResource("slideItemGridL") as Storyboard;
            gridsb.Begin(grid);
        }

        bool NavStatusHidden = false;
        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            addedWidth = this.ActualWidth + 180;
            if (NavStatusHidden)
            {
                RunStoryboardNavLockLeft();
            }
            if (!NavStatusHidden)
            {
                RunStoryboardNavLockRight();
            }

        }

        private void btnShwHideNav_Click(object sender, RoutedEventArgs e)
        {
            if (!NavStatusHidden)
            {
                RunStoryboardNavLeft();
                NavStatusHidden = true;
            }
            else
            {
                RunStoryboardNavRight();
                NavStatusHidden = false;
            }
        }

        private void DoubleAnimation_Completed(object sender, EventArgs e)
        {

        }

        private void btnItem0_Click(object sender, RoutedEventArgs e)
        {
            gridItem1.Visibility = System.Windows.Visibility.Hidden;
            gridItem2.Visibility = System.Windows.Visibility.Hidden;
            gridItem3.Visibility = System.Windows.Visibility.Hidden;
            gridItem4.Visibility = System.Windows.Visibility.Hidden;
            gridItem5.Visibility = System.Windows.Visibility.Hidden;
            gridItem6.Visibility = System.Windows.Visibility.Hidden;
            gridItem7.Visibility = System.Windows.Visibility.Hidden;
            gridItem0.Visibility = System.Windows.Visibility.Visible;
            RunStoryboardGridItemLeft(gridItem0);
        }

        private void btnItem1_Click(object sender, RoutedEventArgs e)
        {
            gridItem0.Visibility = System.Windows.Visibility.Hidden;
            gridItem5.Visibility = System.Windows.Visibility.Hidden;
            gridItem4.Visibility = System.Windows.Visibility.Hidden;
            gridItem3.Visibility = System.Windows.Visibility.Hidden;
            gridItem2.Visibility = System.Windows.Visibility.Hidden;
            gridItem6.Visibility = System.Windows.Visibility.Hidden;
            gridItem7.Visibility = System.Windows.Visibility.Hidden;
            gridItem1.Visibility = System.Windows.Visibility.Visible;
            RunStoryboardGridItemLeft(gridItem1);
        }

        private void btnItem2_Click(object sender, RoutedEventArgs e)
        {
            gridItem0.Visibility = System.Windows.Visibility.Hidden;
            gridItem5.Visibility = System.Windows.Visibility.Hidden;
            gridItem4.Visibility = System.Windows.Visibility.Hidden;
            gridItem3.Visibility = System.Windows.Visibility.Hidden;
            gridItem1.Visibility = System.Windows.Visibility.Hidden;
            gridItem6.Visibility = System.Windows.Visibility.Hidden;
            gridItem7.Visibility = System.Windows.Visibility.Hidden;
            gridItem2.Visibility = System.Windows.Visibility.Visible;
            RunStoryboardGridItemLeft(gridItem2);
        }

        private void btnItem3_Click(object sender, RoutedEventArgs e)
        {
            gridItem0.Visibility = System.Windows.Visibility.Hidden;
            gridItem1.Visibility = System.Windows.Visibility.Hidden;
            gridItem2.Visibility = System.Windows.Visibility.Hidden;
            gridItem4.Visibility = System.Windows.Visibility.Hidden;
            gridItem5.Visibility = System.Windows.Visibility.Hidden;
            gridItem6.Visibility = System.Windows.Visibility.Hidden;
            gridItem7.Visibility = System.Windows.Visibility.Hidden;
            gridItem3.Visibility = System.Windows.Visibility.Visible;
            RunStoryboardGridItemLeft(gridItem3);
        }

        private void btnItem4_Click(object sender, RoutedEventArgs e)
        {
            gridItem0.Visibility = System.Windows.Visibility.Hidden;
            gridItem1.Visibility = System.Windows.Visibility.Hidden;
            gridItem2.Visibility = System.Windows.Visibility.Hidden;
            gridItem3.Visibility = System.Windows.Visibility.Hidden;
            gridItem5.Visibility = System.Windows.Visibility.Hidden;
            gridItem6.Visibility = System.Windows.Visibility.Hidden;
            gridItem7.Visibility = System.Windows.Visibility.Hidden;
            gridItem4.Visibility = System.Windows.Visibility.Visible;
            RunStoryboardGridItemLeft(gridItem4);
        }

        private void btnItem5_Click(object sender, RoutedEventArgs e)
        {
            gridItem0.Visibility = System.Windows.Visibility.Hidden;
            gridItem1.Visibility = System.Windows.Visibility.Hidden;
            gridItem2.Visibility = System.Windows.Visibility.Hidden;
            gridItem3.Visibility = System.Windows.Visibility.Hidden;
            gridItem4.Visibility = System.Windows.Visibility.Hidden;
            gridItem6.Visibility = System.Windows.Visibility.Hidden;
            gridItem7.Visibility = System.Windows.Visibility.Hidden;
            gridItem5.Visibility = System.Windows.Visibility.Visible;
            RunStoryboardGridItemLeft(gridItem5);
        }
        private void btnItem6_Click(object sender, RoutedEventArgs e)
        {
            gridItem0.Visibility = System.Windows.Visibility.Hidden;
            gridItem1.Visibility = System.Windows.Visibility.Hidden;
            gridItem2.Visibility = System.Windows.Visibility.Hidden;
            gridItem3.Visibility = System.Windows.Visibility.Hidden;
            gridItem4.Visibility = System.Windows.Visibility.Hidden;
            gridItem5.Visibility = System.Windows.Visibility.Hidden;
            gridItem7.Visibility = System.Windows.Visibility.Hidden;
            gridItem6.Visibility = System.Windows.Visibility.Visible;
            RunStoryboardGridItemLeft(gridItem6);
        }
        private void btnItem7_Click(object sender, RoutedEventArgs e)
        {
            gridItem0.Visibility = System.Windows.Visibility.Hidden;
            gridItem1.Visibility = System.Windows.Visibility.Hidden;
            gridItem2.Visibility = System.Windows.Visibility.Hidden;
            gridItem3.Visibility = System.Windows.Visibility.Hidden;
            gridItem4.Visibility = System.Windows.Visibility.Hidden;
            gridItem5.Visibility = System.Windows.Visibility.Hidden;
            gridItem6.Visibility = System.Windows.Visibility.Hidden;
            gridItem7.Visibility = System.Windows.Visibility.Visible;
            RunStoryboardGridItemLeft(gridItem7);
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Style _style = null;
            _style = (Style)FindResource("MyStyle");
            this.Style = _style;
        }
        
        private void cefReferrals_FrameLoadEnd(object sender, FrameLoadEndEventArgs e)
        {
            cefReferralsLoaded = true;
        }

        private void cefLogin_FrameLoadEnd(object sender, FrameLoadEndEventArgs e)
        {
            cefLoginLoaded = true;
        }

    }
}
